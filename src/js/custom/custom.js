(function ($) {

	/*==========FOR SVG ==============*/
	svg4everybody({});

	/*========== TRANSITION SCROLL ==============*/

	$('.scroll').on("click", function (e) {
		e.preventDefault();
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 1000);
	});

/*========== HEADER FIXED ==============*/

	$(window).scroll(function(){
			if( $(window).scrollTop() > 400 ) {
					$('.first').addClass('no-menu');
					$('#menu').addClass('fixed');
			} else {
					$('.first').removeClass('no-menu');
					$('#menu').removeClass('fixed');
			}
			if( $(window).scrollTop() > 700 ) {
					$('#menu').addClass('show');
			} else {
					$('#menu').removeClass('show');
			}
	});
	/*----------------------------------------
	 VALIDATIONS JQUERY FORM
	 ----------------------------------------*/
	$(function () {
		/* RULES */
		$("#form-call").validate({
			focusCleanup: true,
			focusInvalid: false
		});
	});

	/*----------------------------------------
	 SLICK CAROUSELS
	 ----------------------------------------*/
	$('.carousel').slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		adaptiveHeight: true
	});
	/*----------------------------------------
	 MOBILE MENU
	 ----------------------------------------*/

	var menu = $('.menu'),
		buttonMenu = $('#btn-menu'),
		closeMenu = $('#btn-close');

	buttonMenu.on('click', function (event) {
		event.preventDefault();
		menu.addClass('open');
		$('body').css('overflow', 'hidden');
	});

	closeMenu.on('click', function (event) {
		event.preventDefault();
		menu.removeClass('open');
		$('body').css('overflow', '');
	});


	/*========== PRESS BUTTON ESC ==============*/

	$(document).keyup(function (event) {
		if (event.keyCode == 27) {
			menu.removeClass('open');
			$('body').css('overflow', '');
		}
	});


})(jQuery);


